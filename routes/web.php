<?php

use App\Http\Controllers\MahasiswaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('template.index');
});

// http://materi.test/mahaiswa/add

Route::prefix('mahasiswa')->group(function() {
    Route::get('/', [MahasiswaController::class, 'index'])->name('mhs.index');
    Route::get('show/{id}', [MahasiswaController::class, 'show'])->name('mhs.show');
    Route::get('add', [MahasiswaController::class, 'create'])->name('mhs.create');
    Route::post('store', [MahasiswaController::class, 'store'])->name('mhs.store');
    Route::get('edit/{id}', [MahasiswaController::class, 'edit'])->name('mhs.edit');
    Route::put('update/{id}', [MahasiswaController::class, 'update'])->name('mhs.update');
    Route::delete('delete/{id}', [MahasiswaController::class, 'destroy'])->name('mhs.delete');
});
