<?php

namespace Database\Seeders;

use App\Models\Mahasiswa;
use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mhs = new Mahasiswa();
        $mhs->create([
            'npm' => '202001020001',
            'nama' => 'Kambing',
            'alamat' => 'Pamekasan'
        ]);
        $mhs->create([
            'npm' => '202001020002',
            'nama' => 'Kucing',
            'alamat' => 'Palengaan'
        ]);
    }
}
