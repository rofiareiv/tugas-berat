<table border="1">
    <tr>
        <th>Npm</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th colspan="3">Aksi</th>
    </tr>
    @foreach ($data as $item)
    <tr>
        <td>{{ $item->npm }}</td>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->alamat }}</td>
        <td><a href="{{ route('mhs.edit', $item->id)}}">Ubah</a></td>
        <td>
            <form method="POST" action="{{ route('mhs.delete', $item->id) }}" id="hapus">
                @csrf
                @method('DELETE')
                <button type="submit">Hapus</button>
            </form>            
        </td>
        <td><a href="{{ route('mhs.show', $item->id) }}">Tampilkan</a></td>
    </tr>        
    @endforeach
</table>
<a href="{{ route('mhs.create')}}">Tambah</a>