<form method="POST" action="{{ route('mhs.update', $data->id)}}">
    @csrf
    @method('PUT')
    <table>
        <tr>
            <td>NPM</td>
            <td><input type="text" name="npm" value="{{ $data->npm }}" /></td>
        </tr>
        <tr>
            <td>Nama</td>
            <td><input type="text" name="nama" value="{{ $data->nama }}" /></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><input type="text" name="alamat" value="{{ $data->alamat }}" /></td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit">Ubah</button></td>
        </tr>
    </table>
</form>